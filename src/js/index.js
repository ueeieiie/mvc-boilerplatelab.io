////////*** MY MODELS ****///////////

    // my Square model
class Square {
	constructor(color, size){
		this.color = color;
		this.size = size;
	}
}
///*** END OF MODELS ****////



////////*** MY VIEWS ****///////////

    // my only View 
let squaresView = document.createElement('div');
    squaresView.style.border = '1px solid black';
    squaresView.style.padding = '25px';

///*** END OF VIEWS ****////



////////*** MY CONTROLLERS ****///////////

    // my main and only Controller
class squareController {  
	constructor(view, model){
		this.view = view;
		this.model = model;
	}
        
        // returns a real square object
	createSquare(){ 
		let square = document.createElement('div');
		square.style.backgroundColor = this.model.color;
		square.style.width = this.model.size + 'px';
		square.style.height = this.model.size + 'px';
		
        return square;
	}

        // appends elements to the view
        // el is a shorthand for element
    appendToView(el){ 
        this.view.appendChild(el);
    }
}
///*** END OF CONTROLLERS ****////



////////*** APP STARTER ****///////////

    //my app goes here
let appContainer = document.querySelector('.app');

    //here I inject my views(for now only 1 view) to the DOM
appContainer.appendChild(squaresView);
    
    //creating new instance of Square
var square = new Square('red', 100);

    //creating a new instance of squareController 
    // with 2 parameters: a view and a model
var app = new squareController(squaresView, square);

    // here I create a real square object which is
    // based on my Square instance
var s = app.createSquare();

    // finally I append to the DOM the element I created
app.appendToView(s);

///*** END OF APP STARTER ****////
