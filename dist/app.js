/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

////////*** MY MODELS ****///////////

// my Square model
var Square = function Square(color, size) {
    _classCallCheck(this, Square);

    this.color = color;
    this.size = size;
};
///*** END OF MODELS ****////


////////*** MY VIEWS ****///////////

// my only View 


var squaresView = document.createElement('div');
squaresView.style.border = '1px solid black';
squaresView.style.padding = '25px';

///*** END OF VIEWS ****////


////////*** MY CONTROLLERS ****///////////

// my main and only Controller

var squareController = function () {
    function squareController(view, model) {
        _classCallCheck(this, squareController);

        this.view = view;
        this.model = model;
    }

    // returns a real square object


    _createClass(squareController, [{
        key: 'createSquare',
        value: function createSquare() {
            var square = document.createElement('div');
            square.style.backgroundColor = this.model.color;
            square.style.width = this.model.size + 'px';
            square.style.height = this.model.size + 'px';

            return square;
        }

        // appends elements to the view
        // el is a shorthand for element

    }, {
        key: 'appendToView',
        value: function appendToView(el) {
            this.view.appendChild(el);
        }
    }]);

    return squareController;
}();
///*** END OF CONTROLLERS ****////


////////*** APP STARTER ****///////////

//my app goes here


var appContainer = document.querySelector('.app');

//here I inject my views(for now only 1 view) to the DOM
appContainer.appendChild(squaresView);

//creating new instance of Square
var square = new Square('red', 100);

//creating a new instance of squareController 
// with 2 parameters: a view and a model
var app = new squareController(squaresView, square);

// here I create a real square object which is
// based on my Square instance
var s = app.createSquare();

// finally I append to the DOM the element I created
app.appendToView(s);

///*** END OF APP STARTER ****////

/***/ })
/******/ ]);
//# sourceMappingURL=app.js.map